const electron = require('electron')
const express = require('./resources/kaerdos.school/app')

electron.app.on('ready', () => {
    express.listen(3000)
    let win = new electron.BrowserWindow({
        width: 800,
        height: 600,
        resizable:false,       
    })

    win.loadURL('localhost:3000')
})